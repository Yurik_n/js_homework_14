const buttonTheme = document.querySelector(".theme")
const bodyNode = document.querySelector("body")

const changeClassOfSelector = (selector) => {
        let htmlElement = Array.from(document.querySelectorAll(selector))
        htmlElement.forEach(element => {
                element.classList.toggle("dark")
        });
}

const changeTheme = () => {
        changeClassOfSelector(".theme")
        changeClassOfSelector("body")
        changeClassOfSelector(".button-get-started")
        changeClassOfSelector(".text")
        changeClassOfSelector(".without-underline")
        changeClassOfSelector("h2")
        buttonTheme.innerHTML = bodyNode.classList.contains("dark") ? "Light theme" : "Dark theme"
}

if (localStorage.getItem("buttonTheme") === "dark") {
        changeTheme()  
} 

buttonTheme.addEventListener("click", () => {
        changeTheme() 
        if (!(localStorage.getItem("buttonTheme") === "dark")) {
                localStorage.setItem("buttonTheme", "dark")    
        }else {
                localStorage.removeItem("buttonTheme")  
        }        
})

























//let localStorageTheme = localStorage.setItem("buttonTheme", "dark")
//localStorage.setItem("buttonTheme", "dark")


// if (localStorage.getItem("buttonTheme") === "dark") {
//         localStorage.removeItem("buttonTheme")
        
//        buttonTheme.innerHTML = "Dark theme"
        
// } else  {
//         localStorage.setItem("buttonTheme", "dark")
       
//         buttonTheme.innerHTML = "Light theme"
// }


// buttonTheme.addEventListener("click", (e) => {
//         localStorage.setItem("buttonTheme", "dark")
//         changeTheme()
// })

//         if (localStorage.getItem("buttonTheme") === "dark") {
//                 buttonTheme.innerHTML = "Dark theme"
//                 localStorage.removeItem("buttonTheme")
                
//                 // changeClassOfSelector(".theme")
//                 // changeClassOfSelector("body")
//                 // changeClassOfSelector(".button-get-started")
//                 // changeClassOfSelector(".text")
//                 // changeClassOfSelector(".without-underline")
//                 //changeClassOfSelector("h2")
//                 changeTheme()
               
//         } else {
//                 localStorage.setItem("buttonTheme", "dark")

//                 buttonTheme.innerHTML = "Light theme"
                
                
                
//                 // changeClassOfSelector(".theme")
//                 // changeClassOfSelector("body")
//                 // changeClassOfSelector(".button-get-started")
//                 // changeClassOfSelector(".text")
//                 // changeClassOfSelector(".without-underline")
//                 // changeClassOfSelector("h2")
//                changeTheme()
//         }

//         console.log(e);






